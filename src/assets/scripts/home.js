/* requires:
	lightgallery.js
	jquery.bxslider.min.js
	jquery.waypoints.js
	jquery.validationEngine.js
	jquery.validationEngine-es.js
	lg-fullscreen.js
	lg-zoom.js
	lg-video.js
*/
$(()=>{
	//code here
	$('.video__slider_content').lightGallery({
		selector: 'a'
	});
	  $('.trigger').click(function() {
	     $('.modal-wrapper').toggleClass('open');
	    $('.page-wrapper').toggleClass('blur');
	     return false;
	  });


		$('.slidr--home_js').bxSlider({
			captions: true,
			controls: true,
			pager: false,
			nextSelector: '#slider_home--next',
			prevSelector: '#slider_home--prev',
			nextText: '',
			prevText: ''
		});


	
})