/* requires:
	jquery.bxslider.min.js
*/
$(()=>{
	//code here
	$('.b1-bxslider').bxSlider({
		mode: 'fade',
		controls:true,
		pager:true,
		adaptiveHeight:true,
		auto:false,
		onSliderLoad: function(currentIndex) {
		$('.b1-bxslider').children().eq(currentIndex).addClass('active');
		},
		onSlideBefore: function($slideElement){
		$('.b1-bxslider').children().removeClass('active');
		setTimeout( function(){
		$slideElement.addClass('active');
		},500);
		}
	});


})