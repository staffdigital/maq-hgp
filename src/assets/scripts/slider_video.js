/* requires:
	jquery.bxslider.min.js
*/

$(()=>{
	// console.log("que sucedeeeee");
	$('.video__slider__pitcher').bxSlider({
		// mode: 'fade',
		auto: false,
		adaptiveHeight: true,
		pause: '5000',
		speed: '2000',
		controls: true,
		prevText: '<span class="video__slider__icono__prev icon-prev"><span/></span>',
		nextText: '<span class="video__slider__icono__next icon-next"></span>',
		pager: false,
		onSliderLoad: function(currentIndex) {
			$('.video__slider__pitcher').children().eq(currentIndex+1).addClass('active')
		},
		onSlideBefore: function($slideElement){
			$('.video__slider__pitcher').children().removeClass('active');
			setTimeout( function(){
				$slideElement.addClass('active');
			},500);
		}
	});
})