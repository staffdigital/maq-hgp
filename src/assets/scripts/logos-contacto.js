/* requires:

*/

$(()=>{
	$('.logos-content-slider').slick({
		dots: true,
		infinite: true,
		arrows: false,
		speed: 300,
		fade: false,
		slidesToShow: 1,
		adaptiveHeight: true
	});
})