/* requires:
	
*/
$(()=>{
	$('.modal__2__open').click(function(event) {
		event.preventDefault();
		$('.modal__2').addClass('active');
	});
	$('.modal__2__close, .modal__2__overlay').click(function(event) {
		event.preventDefault();
		$('.modal__2').removeClass('active');
	});	
})